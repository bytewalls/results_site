<?php
/**
 * @Author: Jordan Oroshiba
 * @Date:   2016-05-04 11:10:59
 * @Last Modified by:   Jordan Oroshiba
 * @Last Modified time: 2016-05-04 21:22:14
 */
  printHeader( 'Home', $title, $base_url, $isLoggedIn );

  if($isLoggedIn){
    echo "<a href='?page=addRace'>Add Race</a><br>";
  }
  printRaces($pdo, $db_prefix);
?>