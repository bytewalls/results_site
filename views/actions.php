<?php
/**
 * @Author: Jordan Oroshiba
 * @Date:   2016-05-04 11:22:23
 * @Last Modified by:   Jordan Oroshiba
 * @Last Modified time: 2016-05-05 23:10:48
 */
$action = $_GET['action'];
if( $isLoggedIn ){
  if( $action == 'addRaceDB' )
    addRaceDB($pdo, $db_prefix, $_POST['raceName'], $_POST['tableName'], $_POST['date']);
  else if( $action == 'updateEvents' ){
    updateEvents($pdo, $db_prefix, $_POST['tableName'], $_POST['events']);
  }
  else if ( $action == 'drop'){
    deleteResults($pdo, $db_prefix, $_GET['race'], $_GET['event']);
  }
  else if( $action == 'uploadResults'){
    importResults($pdo, $db_prefix, $_POST['tableName'], $_POST['eventName']);
  }
}                
?>