<?php
/**
 * @Author: Jordan Oroshiba
 * @Date:   2016-05-04 21:55:30
 * @Last Modified by:   Jordan Oroshiba
 * @Last Modified time: 2016-05-04 22:02:12
 */
include (dirname(__FILE__).'/core/config.php');

$pdo = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass, array(PDO::ATTR_PERSISTENT=> true));
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$page = $_GET['page'];
include (dirname(__FILE__).'/core/pages.php');
  //depending on the pages we pull specific information

  //login page needs the login form
  if( $page == 'login' ){
    printLogin($base_url);
  }
  //Race pages are broken into multiple layers
  else if( $page == 'Race' ){
    //first have we set a specific race?
    if( isset($_GET['race']) ){
      $raceTable = $_GET['race'];

      //if the event is set logged in users will get an option to upload
      //results and everyone will see any current results
      if( isset($_GET['event']) ) {
        $event = $_GET['event'];
        if ( isset($_GET['search']) ){
          printSearchResults( $pdo, $db_prefix, $raceTable, $event, $_GET['search']);
        }
        else if ( isset($_GET['cat'])){
          printCategoryResults( $pdo, $db_prefix, $raceTable, $event, $_GET['cat']);
        }
        else if ( isset($_GET['sex'])){
          printGenderResults( $pdo, $db_prefix, $raceTable, $event, $_GET['sex']);
        }
        else {
          printFullResults( $pdo, $db_prefix, $raceTable, $event);
        }
      }
    }
    //if no race selected, need to go back to main page
    else {
      redirect($base_url);
    }
  }

  //pages only available to logged in users
?>