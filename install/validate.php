<?php
/**
 * @Author: Jordan Oroshiba
 * @Date:   2015-09-22 10:27:02
 * @Last Modified by:   Jordan Oroshiba
 * @Last Modified time: 2016-05-04 21:26:22
 */
//If email and password confirmations add up we will process the data, otherwise we will redo the form
$email = $_POST['email'];
$emailConfirm = $_POST['emailConfirm'];
$password = $_POST['password'];
$passwordConfirm = $_POST['passwordConfirm'];
if ($email==$emailConfirm&&$password==$passwordConfirm){
	//go ahead and require and connect to db if these things happen
	//no reason to bother otherwise
	include_once('../core/config.php');
	//include_once('../core/connect.php');
	//connect($db_host,$db_user,$db_pass,$db_name);
   $pdo = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass, array(PDO::ATTR_PERSISTENT=> true));
   $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPT

	$fName = $_POST['fName'];
	$lName = $_POST['lName'];
	$username = $_POST['username'];
	$salt = '$1$'.substr(number_format(time() * rand(),0,'',''),0,12);
	$password_encrypted = crypt($password,$salt);
   $instance=number_format(time() * rand(),0,'','');
   $token=substr(number_format(time() * rand(),0,'',''),0,20);
   $tokenCrypt=md5($token);

	$query_createAdmin = "INSERT INTO ".$db_prefix."_users 
                        VALUES ('$username', '$email', '$fName', '$lName', '$password_encrypted', '$salt', '$tokenCrypt' , '$instance' ,'admin' )";
	$pdo->query($query_createAdmin) or die('Unmitigated Distater in Code	'.$password_encrypted.'	 '.$salt.'<br>'.$query_createAdmin);
	fopen('complete.txt','w') or die('fail');
	header('Location:'.$base_url);
	exit();
}
?>
<!--Otherwise we will reload the form and try again-->
<!DOCTYPE html>
<html>
   <head>
      <title>AllSports Results Configuration - User</title>
   </head>
   <body>
   	<h2>Admin User Config</h2>
   	<h3>EMAIL OR PASSWORDS DID NOT MATCH, TRY AGAIN</h3>
   	<form action='validate.php' method='post'>
   		First Name: <input type='text' name='fName'><br/>
   		Last Name: <input type='text' name='lName'><br/>
   		Email: <input type='text' name='email'><br/>
   		Confirm Email: <input type='text' name='emailConfirm'><br/>
   		Username: <input type='text' name='username'><br/>
   		Password: <input type='password' name='password'><br/>
   		Password Confirm: <input type='password' name='passwordConfirm'><br/>
   		<input type='submit'>
   	</form>
   </body>
</html>
