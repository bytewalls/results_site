<?php
/**
 * @Author: Jordan Oroshiba
 * @Date:   2015-09-22 09:29:04
 * @Last Modified by:   Jordan Oroshiba
 * @Last Modified time: 2016-05-04 21:26:05
 */
//import config file and connect to db
require_once('../core/config.php');
require_once('../core/connect.php');

$pdo = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass, array(PDO::ATTR_PERSISTENT=> true));
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if($pdo){
   //SQL query to create the various databases and then complete the queries.
   $query_usersCreate = 'CREATE TABLE '.$db_prefix.'_users(
                        username VARCHAR(50),
                        email VARCHAR(128),
                        fName VARCHAR(50),
                        lName VARCHAR(50),
                        pass_hash CHAR(34),
                        salt CHAR(15),
                        token VARCHAR(100),
                        instance VARCHAR(100),
                        access VARCHAR(7))';
   $query_racesCreate = 'CREATE TABLE '.$db_prefix.'_races(
                        raceName VARCHAR(200),
                        tableName VARCHAR(100),
                        eventNames VARCHAR(300),
                        date DATE)';
   $pdo->query($query_usersCreate);
   $pdo->query($query_racesCreate) or die();
   ?>
   <!-- Output form to create an admin user, otherwise no one can access anything -->
   <!DOCTYPE html>
   <html>
      <head>
         <title>Registration - User</title>
      </head>

      <body>
         <h2>AllSports Results Configuration - User</h2>
         <h3>Database configuration = success!</h3>
         <form action='validate.php' method='post'>
         	First Name: <input type='text' name='fName'><br/>
         	Last Name: <input type='text' name='lName'><br/>
         	Email: <input type='text' name='email'><br/>
         	Confirm Email: <input type='text' name='emailConfirm'><br/>
         	Username: <input type='text' name='username'><br/>
         	Password: <input type='password' name='password'><br/>
         	Password Confirm: <input type='password' name='passwordConfirm'><br/>
         	<input type='submit'>
         </form>
      </body>
   </html>
<?php
}
else{
?>
<!DOCTYPE html>
<html>
   <head>
      <title>Error Config</title>
   </head>

   <body>
      <h2>ERROR IN CONFIG FILE< CANNOT CONNECT TO DB, PLEASE FIX</h2>
   </body>
</html>
<?php
}
?>
