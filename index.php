
<?php
/**
 * @Author: Jordan Oroshiba
 * @Date:   2015-09-22 09:28:39
 * @Last Modified by:   Jordan Oroshiba
 * @Last Modified time: 2016-05-05 23:31:13
 */
include (dirname(__FILE__).'/core/config.php');
/* commenting out this block for now, so I can leave files there while testing.
if( is_dir( 'INSTALL' ) ) {
  if( file_exists( 'INSTALL/complete.txt' ) ) {
    echo "<h2>Please delete folder INSTALL</h2>";
    exit();
  }
  header( 'Location: INSTALL/index.php' );
}*/

$pdo = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass, array(PDO::ATTR_PERSISTENT=> true));
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

//Include the main functions every page will need access to
//also gets login information before any headers have been sent
include (dirname(__FILE__).'/core/main.php') ;

//always need to check if logged in and such, and always do it before we get headers or any page information
if(isset($_GET['main']) && $_GET['main'] == 'login' ){
  checkLogin($pdo, $db_prefix, $_POST['username'], $_POST['password'], $base_url);
}
if( isset($_GET['main']) && $_GET['main'] == 'logout'){
  logout($base_url);
}

//will be false if not logged in and contain access if not logged in
$isLoggedIn = isLoggedIn($pdo, $db_prefix, $base_url);

//Everything else depends on the page below is the controller for views
//Each view has access to some different functions
if( !isset( $_GET['page'] ) && !isset( $_GET['action'] ) ){
  include( dirname(__FILE__).'/views/home.php');
}

// when not on the homepage we need to get the page info, and print out
// appropriate header
else if ( isset($_GET['page']) ){
  $page = $_GET['page'];
  //we only need the content functions if there is a page decleration
  include (dirname(__FILE__).'/core/page_func.php');
  include ( dirname(__FILE__)."/views/$page.php");
}

//there are some actions which can be done, they do things
//they do not affect things the user sees
else if( isset( $_GET['action']) ){
  //only need action functions if actions is called
  include (dirname(__FILE__).'/core/action_func.php');
  include ( dirname(__FILE__).'/views/actions.php');
}
echo "</div></body>";
?>
