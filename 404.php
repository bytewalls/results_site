<?php
/**
 * @Author: Jordan Oroshiba
 * @Date:   2016-01-27 17:29:28
 * @Last Modified by:   Jordan Oroshiba
 * @Last Modified time: 2016-05-05 23:43:12
 */

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset='utf-8'>
  <meta name='viewport' content='width=device-width, initial-scale=1'>
  <!-- Latest compiled and minified CSS -->
  <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' 
  integrity='sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7' crossorigin='anonymous'>

  <!-- Optional theme -->
  <!--<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css' 
  integrity='sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r' crossorigin='anonymous'>-->
  
  <!--<link rel='stylesheet' href='bootstrap/css/bootstrap.min.css'>-->
  <link rel='stylesheet' href='styles/style.css'>
  <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js'></script>
  <!--<script src='bootstrap/js/bootstrap.min.js'></script>-->
  <!-- Latest compiled and minified JavaScript -->
  <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js' 
  integrity='sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS' crossorigin='anonymous'></script>
  <title> 404 Not Found!! </title>
</head>
<body>
  <div class="error">
    <div class="error-code m-b-10 m-t-20">404 <i class="fa fa-warning"></i></div>
    <h3 class="font-bold">We couldn't find the page..</h3>

    <div class="error-desc">
      Sorry, but the page you are looking for was either not found or does not exist. <br/>
      Try refreshing the page or click the button below to go back to the Homepage.
      <div>
          <a class=" login-detail-panel-button btn" href="/results/">
            <i class="fa fa-arrow-left"></i>
            Go back to Homepage                        
          </a>
      </div>
    </div>
  </div>
  </body>