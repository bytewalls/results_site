/*
* @Author: Jordan Oroshiba
* @Date:   2016-05-04 21:20:15
* @Last Modified by:   Jordan Oroshiba
* @Last Modified time: 2016-05-04 23:22:55
*/
function getSearch( race, event, base_url)
{

  var xhttp = new XMLHttpRequest();
  var search = document.getElementById("search").value;
  var currentState = base_url+"/?page=Race&race="+race+"&event="+event;

  if( search != '' )
    window.history.pushState({urlPath:currentState+"&search="+search},'',currentState+"&search="+search);
  else
    window.history.pushState({urlPath:currentState},'',currentState);

  xhttp.onreadystatechange = function()
  {
    document.getElementById("results").innerHTML = xhttp.responseText;
  }
  xhttp.open("GET", "updates.php?page=Race&race="+race+"&event="+event+"&search="+search);
  xhttp.send();
}
'use strict';