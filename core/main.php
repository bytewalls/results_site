<?php
/**
 * @Author: Jordan Oroshiba
 * @Date:   2015-09-24 10:01:20
 * @Last Modified by:   Jordan Oroshiba
 * @Last Modified time: 2016-05-05 23:30:01
 */

/**
 * checkLogin -- Checks to see if provided user has logged in properly
 * @param  PDO    $pdo          database connection
 * @param  string $db_prefix    database prefix for install
 * @param  string $user_toCheck user input from the form for username
 * @param  string $pass_toCheck user input from form for password
 * @param  string $base_url     base url for install
 * @return none               
 */
function checkLogin ($pdo, $db_prefix, $user_toCheck, $pass_toCheck, $base_url){
  //check to see if the remember was set
  if(isset($_POST['remember'])){
    $remember=true;
  }
  else{
    $remember=false;
  }

  //db query for the user
  $table = preg_replace("/[^A-Za-z0-9_]/", '', $db_prefix."_users");
  $pdo_preped = $pdo->prepare("SELECT * FROM `{$table}` 
                               WHERE username = ?;");
  $params = array ($user_toCheck);
  $pdo_preped->execute($params);
  $result = $pdo_preped->fetch();
  //Only continue if found a SINGLE user
  if( is_array($result) ){
    $calc_hash = crypt($pass_toCheck, $result['salt']);

    if($calc_hash === $result['pass_hash']){

      $instance=number_format(time() * rand(),0,'','');
      $token=substr(number_format(time() * rand(),0,'',''),0,20);
      $tokenCrypt=md5($token);

      $pdo_preped = $pdo->prepare("UPDATE `{$table}` SET token=? , instance=? WHERE username=?;");
      $params = array($tokenCrypt,$instance,$user_toCheck);
      $pdo_preped->execute($params);

      $cookie_user= $user_toCheck.','.$result['access'];
      $cookie_array[0] = $user_toCheck;
      $cookie_array[1] = $token;
      $cookie_array[2] = $instance;
      $cookieString = implode(',', $cookie_array);

      if($remember){
        $time = time()+1209600;
        setcookie('remember','',$time);
      }
      else {
        $time = 0;
      }
  
      setcookie( 'login', $cookieString, $time);
      redirect($base_url);
    }
  }

  //Only way to get here is if login failed either wrong pass or user
  redirect($base_url.'?page=login&note=failed');
}

/******************************************************************************
** Function: isLoggedIn
** Parameters: $base_url
** Does: Checks if user is already logged in, returns true if so. Sets a long
**       cookie for the login if so.
*******************************************************************************/
function isLoggedIn($pdo, $db_prefix, $base_url){
  //prep PDO to get data from table
  $table = preg_replace("/[^A-Za-z0-9_]/", '', $db_prefix."_users");
  $pdo_preped = $pdo->prepare("SELECT * FROM `{$table}` 
                                 WHERE username=?;");

  if(isset($_COOKIE['login'])){
    $cookie_array = explode(',',$_COOKIE['login']);

    //get the user data from the table
    $params = array($cookie_array[0]);
    $pdo_preped->execute($params);
    $result = $pdo_preped->fetch();

    //prep pdo for a token update
    $pdo_preped = $pdo->prepare("UPDATE `{$table}` 
                                    SET token=? 
                                    WHERE username=?;");

    //if all three (user, instance, token) match then have a valid cookie
    if ($result['username']===$cookie_array[0]
        && $result['instance']===$cookie_array[2]
        && $result['token']===md5($cookie_array[1])){

      //create a new token for the db. This will change at every page refresh
      //making it very hard to crack while being used
      $new_tokenCookie = number_format(time() * rand(),0,'','');
      $new_tokenDB=md5($new_tokenCookie);
      $cookie_array[1] = $new_tokenCookie;

      if(isset($_COOKIE['remember'])){
        $time = time()+1209600;
        setcookie('remember','',$time);
      }
      else {
        $time = 0;
      }

      //set new cookie
      setcookie('login',implode(',',$cookie_array),$time);

      //update database
      $params=array($new_tokenDB, $cookie_array[0]);
      $pdo_preped->execute($params);

      return $result['access'];
    }
    //This user has likely had their token stolen but the instance, delete
    //token so it cannot be brute forced TODO: more or less same with instance maybe?
    else if($result['username']===$cookie_array[0]
            && $result['instance']===$cookie_array[2]){
      $params=array('',$cookie_array[0]);
      $pdo_preped->execute($params);
    }
  }

  //if get this far they were not given access, delete their cookies and such 
  //and return false
  else{
    return false;
  }
}

/**
 * unauthorizedAccess -- prints out warning message about page. TODO: Make log
 *                       of attempts to access unauthorized pages.
 * @param  string $requiredLevel level of access required to view the page
 * @return none                
 */
function unauthorizedAccess($requiredLevel){
  echo "<h2> Sorry this page is only available those who have $requiredLevel or
             higher access. </h2>";
}

/**
 * logout -- logs user out by deleting cookies and sessions
 * @param  string $base_url base url for the site
 * @return none           
 */
function logout($base_url){
  setcookie('login', '', time()-3600);
  setcookie('remember', '', time()-3600);
  session_destroy();
  redirect($base_url);
}

//redirects back to a specific url

function redirect($url){
    if (headers_sent()){
      die('<script type="text/javascript">window.location.href="'. $url .'";</script>');
    }
		else{
      header('Location: ' . $url);
      die();
    }
}

/******************************************************************************
** Function: printHeader
** Parameters: $page (page user is on), $title (title of page), $base_url
** Does: Prints the correct header for the user, and the page
*******************************************************************************/
function printHeader( $page, $title, $base_url, $isLoggedIn){
   include('config.php');
   echo"<!DOCTYPE html>
     	  <html>
     	  <head>
          <meta charset='utf-8'>
          <meta name='viewport' content='width=device-width, initial-scale=1'>
          <!-- Latest compiled and minified CSS -->
          <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' 
          integrity='sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7' crossorigin='anonymous'>

          <!-- Optional theme -->
          <!--<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css' 
          integrity='sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r' crossorigin='anonymous'>-->
          
          <!--<link rel='stylesheet' href='bootstrap/css/bootstrap.min.css'>-->
          <link rel='stylesheet' href='styles/style.css'>
          <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js'></script>
          <!--<script src='bootstrap/js/bootstrap.min.js'></script>-->
          <!-- Latest compiled and minified JavaScript -->
          <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js' 
          integrity='sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS' crossorigin='anonymous'></script>
          <script src='scripts/ajax.js'></script>
          <title> $page | $title </title>
     	  </head>
     	  <body>
            <nav class='navbar-fixed-top navbar-inverse'>
              <div class='container'>
                <div class='navbar-header'>
                  <a class='navbar-brand' href=' $base_url '>$title</a>
                  <button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#navbar' aria-expanded='false' aria-controls='navbar'>
                    <span class='sr-only'>Toggle navigation</span>
                    <span class='icon-bar'></span>
                    <span class='icon-bar'></span>
                    <span class='icon-bar'></span>
                  </button>
                </div>
                <div id='navbar' class='collapse navbar-collapse'>
                <ul class='nav navbar-nav'>
                  <li";
                  if ($page == 'Home')
                    echo " class='active' ";
                  echo "><a href=' $base_url '>Home</a></li>";
                  if ($page == 'Race')
                    echo "<li class ='active'><a href='$base_url?page=Race&race=".$_GET['race']."''>Results</a></li>";
                  else
                    echo "<li><a href='$base_url'>Results</a></li>";
                  echo "
                </ul>
                <ul class='nav navbar-nav navbar-right'>";
                if( !$isLoggedIn ){
                  //echo "<li><a href ='$base_url?page=login'>Login</a></li>";
                  echo '
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Login <span class="caret"></span></a>
                            <ul id="login-dp" class="dropdown-menu">
                              <li>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <form class="form" role="form" method="post" action="?main=login" id="login-nav">
                                          <div class="form-group">
                                             <label class="sr-only" for="username">Email address</label>
                                             <input type="text" class="form-control" name="username" id="username" placeholder="Username" required>
                                          </div>
                                          <div class="form-group">
                                             <label class="sr-only" for="password">Password</label>
                                             <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
                                          </div>
                                          <div class="form-group">
                                             <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                                          </div>
                                          <div class="checkbox">
                                             <label>
                                             <input type="checkbox" name="remember" checked> keep me logged-in
                                             </label>
                                          </div>
                                       </form>
                                    </div>
                                 </div>
                              </li>
                            </ul>
                          </li>
                        </ul>
                        </div>';
                }
                else {
                  echo "<li><a href='$base_url?main=logout'>Logout</a></li>";
                }
              echo "</li>
            </nav>
            <div class='container'>";

}

/******************************************************************************
** Function: printRaces
** Parameters: $db_prefix
** Does: Prints out a list of links to all races in the DB
** TODO: add pagination
*******************************************************************************/
function printRaces( $pdo, $db_prefix ) {
   //query database for basic races information
   $query = "SELECT *  FROM ".$db_prefix."_races";

   echo "<ul id='racelist'>";
   foreach( $pdo->query($query) as $race ){
      $date = date('m/d/y', strtotime( $race['date'] ) );
      echo "<li><a href='?page=Race&race=".$race['tableName']."'>
           ".$race['raceName']." - ".$date."</a></li>";
   }
   echo "</ul>";
}
?>
