<?php
/**
 * @Author: Jordan Oroshiba
 * @Date:   2016-05-02 18:45:20
 * @Last Modified by:   Jordan Oroshiba
 * @Last Modified time: 2016-05-04 21:25:28
 */
/******************************************************************************
** Function: isLoggedIn
** Parameters: $base_url
** Does: Checks if user is already logged in, returns true if so. Sets a long
**       cookie for the login if so.
** TODO: make work with sessions and not only cookies
*******************************************************************************/
function isLoggedIn($base_url){
   if(isset($_COOKIE['login'])){
      $cookieArray = explode($_COOKIE['login'], ',');
      if (!headers_sent()){
        setcookie('login',implode($cookieArray,','), time()+604800, $base_url);
      }
      return true;
   }
   else{
      return false;
   }
}

function logout($base_url){
  setcookie('login', '', time()-3600, $base_url);
  redirect($base_url);
}

//redirects back to a specific url

function redirect($url){
    if (headers_sent()){
      die('<script type="text/javascript">window.location.href="'. $url .'";</script>');
    }
    else{
      header('Location: ' . $url);
      die();
    }
}

/******************************************************************************
** Function: printHeader
** Parameters: $page (page user is on), $title (title of page), $base_url
** Does: Prints the correct header for the user, and the page
*******************************************************************************/
function printHeader( $page, $title, $base_url, $isLoggedIn){
   include('config.php');
   echo"<!DOCTYPE html>
        <html>
        <head>
          <meta charset='utf-8'>
          <meta name='viewport' content='width=device-width, initial-scale=1'>
          <link rel='stylesheet' href='bootstrap/css/bootstrap.min.css'>
          <link rel='stylesheet' href='styles/style.css'>
          <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js'></script>
          <script src='bootstrap/js/bootstrap.min.js'></script>";
  echo '<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://allsportstiming.com/?q=blogapi/rsd" />
<script type="text/javascript" src="http://allsportstiming.com/themes/theme530/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="http://allsportstiming.com/themes/theme530/build/animation/animation-min.js"></script>
<link rel="stylesheet" type="text/css" href="http://allsportstiming.com/themes/theme530/build/menu/assets/skins/sam/menu.css"/>
<script type="text/javascript" src="http://allsportstiming.com/themes/theme530/build/container/container_core-min.js"></script>
<script type="text/javascript" src="http://allsportstiming.com/themes/theme530/build/menu/menu-min.js"></script>
<script type="text/javascript">
            //<![CDATA[
            // Initialize and render the menu bar when it is available in the DOM
            YAHOO.util.Event.onContentReady("productsandservices", function () {
            var oMenuBar = new YAHOO.widget.MenuBar("productsandservices", { 
                                                            autosubmenudisplay: true, 
                                                            hidedelay: 750, 
                                                            lazyload: true });
          var oSubmenuData = [
          
{
id: "Home",
itemdata: [
]},
{
id: "Clients",
itemdata: [
{ text: "Our Process", url: "http://allsportstiming.com/?q=node/62" },
{ text: "Feedback Form", url: "http://allsportstiming.com/?q=node/95" },
{ text: "New Race Form", url: "http://allsportstiming.com/?q=node/86" },
{ text: "Online Registration Form", url: "http://allsportstiming.com/?q=node/96" }
]},
{
id: "Results",
itemdata: [
{ text: "Chip Return", url: "http://allsportstiming.com/?q=node/78" },
{ text: "Results", url: "http://allsportstiming.com/?q=node/61" }
]},
{
id: "Learning",
itemdata: [
{ text: "Registration Management", url: "http://allsportstiming.com/?q=node/68" }
]},
{
id: "Quote Request",
itemdata: [
]},
{
id: "Contact",
itemdata: [
]},
          ];var ua = YAHOO.env.ua,
                    oAnim;  // Animation instance
                function onSubmenuBeforeShow(p_sType, p_sArgs) {
                    var oBody,
                        oElement,
                        oShadow,
                        oUL;
                    if (this.parent) {
                        oElement = this.element;
                        oShadow = oElement.lastChild;
                        oShadow.style.height = "0px";
                        if (oAnim && oAnim.isAnimated()) {
                            oAnim.stop();
                            oAnim = null;
                        }
                        oBody = this.body;
                        //  Check if the menu is a submenu of a submenu.
                        if (this.parent && 
                            !(this.parent instanceof YAHOO.widget.MenuBarItem)) {
                            if (ua.gecko) {
                                oBody.style.width = oBody.clientWidth + "px";
                            }
                            if (ua.ie == 7) {
                                oElement.style.width = oElement.clientWidth + "px";
                            }
                        }
                        oBody.style.overflow = "hidden";
                        oUL = oBody.getElementsByTagName("ul")[0];
                        oUL.style.marginTop = ("-" + oUL.offsetHeight + "px");
                    }
                }

               function onTween(p_sType, p_aArgs, p_oShadow) {
                    if (this.cfg.getProperty("iframe")) {
                        this.syncIframe();
                    }
                    if (p_oShadow) {
                        p_oShadow.style.height = this.element.offsetHeight + "px";
                    }
                }
                function onAnimationComplete(p_sType, p_aArgs, p_oShadow) {
                    var oBody = this.body,
                        oUL = oBody.getElementsByTagName("ul")[0];
                    if (p_oShadow) {
                        p_oShadow.style.height = this.element.offsetHeight + "px";
                    }
                    oUL.style.marginTop = "";
                    oBody.style.overflow = "";
                    //  Check if the menu is a submenu of a submenu.
                    if (this.parent && 
                        !(this.parent instanceof YAHOO.widget.MenuBarItem)) {
                        // Clear widths set by the "beforeshow" event handler
                        if (ua.gecko) {
                            oBody.style.width = "";
                        }
                        if (ua.ie == 7) {
                            this.element.style.width = "";
                        }
                    }
                }
                function onSubmenuShow(p_sType, p_sArgs) {
                    var oElement,
                        oShadow,
                        oUL;
                    if (this.parent) {
                        oElement = this.element;
                        oShadow = oElement.lastChild;
                        oUL = this.body.getElementsByTagName("ul")[0];
                        oAnim = new YAHOO.util.Anim(oUL, 
                            { marginTop: { to: 0 } },
                            .5, YAHOO.util.Easing.easeOut);
                        oAnim.onStart.subscribe(function () {
                            oShadow.style.height = "100%";
                        });
                        oAnim.animate();
                        if (YAHOO.env.ua.ie) {
                            oShadow.style.height = oElement.offsetHeight + "px";
                            oAnim.onTween.subscribe(onTween, oShadow, this);
                        }
                        oAnim.onComplete.subscribe(onAnimationComplete, oShadow, this);
                    }
                }oMenuBar.subscribe("beforeRender", function () {
                    if (this.getRoot() == this) {this.getItem(1).cfg.setProperty("submenu", oSubmenuData[1]);
this.getItem(2).cfg.setProperty("submenu", oSubmenuData[2]);
this.getItem(3).cfg.setProperty("submenu", oSubmenuData[3]);
}}); oMenuBar.subscribe("beforeShow", onSubmenuBeforeShow);
                oMenuBar.subscribe("show", onSubmenuShow);
oMenuBar.render();            
            });
            //]]>
            </script>
<link rel="alternate" type="application/rss+xml" title="Timing Company | Chip Timing | AllSports Timing | Texas RSS" href="http://allsportstiming.com/?q=rss.xml" />
<link rel="shortcut icon" href="http://allsportstiming.com/misc/favicon.ico" type="image/x-icon" />
  <link type="text/css" rel="stylesheet" media="all" href="http://allsportstiming.com/modules/aggregator/aggregator.css?E" />
<link type="text/css" rel="stylesheet" media="all" href="http://allsportstiming.com/modules/book/book.css?E" />
<link type="text/css" rel="stylesheet" media="all" href="http://allsportstiming.com/modules/node/node.css?E" />
<link type="text/css" rel="stylesheet" media="all" href="http://allsportstiming.com/modules/poll/poll.css?E" />
<link type="text/css" rel="stylesheet" media="all" href="http://allsportstiming.com/modules/system/defaults.css?E" />
<link type="text/css" rel="stylesheet" media="all" href="http://allsportstiming.com/modules/system/system.css?E" />
<link type="text/css" rel="stylesheet" media="all" href="http://allsportstiming.com/modules/system/system-menus.css?E" />
<link type="text/css" rel="stylesheet" media="all" href="http://allsportstiming.com/modules/ubercart/shipping/uc_quote/uc_quote.css?E" />
<link type="text/css" rel="stylesheet" media="all" href="http://allsportstiming.com/modules/ubercart/uc_order/uc_order.css?E" />
<link type="text/css" rel="stylesheet" media="all" href="http://allsportstiming.com/modules/ubercart/uc_product/uc_product.css?E" />
<link type="text/css" rel="stylesheet" media="all" href="http://allsportstiming.com/modules/ubercart/uc_store/uc_store.css?E" />
<link type="text/css" rel="stylesheet" media="all" href="http://allsportstiming.com/modules/user/user.css?E" />
<link type="text/css" rel="stylesheet" media="all" href="http://allsportstiming.com/sites/all/modules/twitter_profile_widget/twitter_profile_widget.css?E" />
<link type="text/css" rel="stylesheet" media="all" href="http://allsportstiming.com/modules/forum/forum.css?E" />
<link type="text/css" rel="stylesheet" media="all" href="http://allsportstiming.com/themes/theme530/style.css?E" />
  <script type="text/javascript" src="http://allsportstiming.com/sites/all/modules/jquery_update/replace/jquery.min.js?E"></script>
<script type="text/javascript" src="http://allsportstiming.com/misc/drupal.js?E"></script>
<script type="text/javascript" src="http://allsportstiming.com/sites/all/modules/twitter_profile_widget/js/widget.js?E"></script>
<script type="text/javascript" src="http://allsportstiming.com/misc/tableheader.js?E"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, { "basePath": "/" });
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
if (Drupal.jsEnabled) { $(document).ready(function() { $("body").addClass("yui-skin-sam"); } ); };
//--><!]]>
</script>
    
  <script type="text/javascript" src="http://info.template-help.com/files/ie6_warning/ie6_script.js"></script>
  <link href="http://allsportstiming.com/themes/theme530/menu.css" media="all" rel="stylesheet" type="text/css" />
  
    <script type="text/javascript" src="http://allsportstiming.com/themes/theme530/js/tabs.js"></script>
  
  <script src="http://allsportstiming.com/themes/theme530/js/loopedslider.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function(){
            $("#loopedSlider").loopedSlider();
        });
    </script>
    <script type="text/javascript" src="http://allsportstiming.com/themes/theme530/js/imagepreloader.js"></script>
  <script type="text/javascript">
        preloadImages([
            "http://allsportstiming.com/themes/theme530/images/bg-li-active.gif"
      ]);
    </script>';

  echo "<title> $page | $title </title>
        </head>
        <body>";
          //More Dad's header crap
  echo '
  <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=413773365344701";
  fjs.parentNode.insertBefore(js, fjs);
}(document, "script", "facebook-jssdk"));</script>

  <div class="min-width">
      <div class="bg-top">
          <div class="bg-slider-shadow">
                <div id="main"><div id="header">
                        <div class="head-row1">
                          <div class="col1">
                                                          </div>
                            <div class="col2">
                              <strong>New User?</strong>&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://allsportstiming.com?q=user/register">Register</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                              if( !$isLoggedIn ){echo "<a href ='$base_url?page=login'>Login</a></li>";}
                              else {echo "<li><a href='$base_url?page=logout'>Logout</a></li>";}
                             echo ' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://allsportstiming.com?q=node/51">Help</a><br />
                            </div>
                        </div>
                        <div class="head-row2">
                            <div class="col1">
                                                                    <a href="http://allsportstiming.com/" title="Home"><img src="http://allsportstiming.com/sites/default/files/theme530_logo.jpg" alt="Home" class="logo" /></a>
                                                                                                                                                            </div>
                            <div class="col2">
                                <div class="search-box">
                                    <form action="/"  accept-charset="UTF-8" method="post" id="search-theme-form">
<div><input type="text" maxlength="128" name="search_theme_form" id="edit-search-theme-form-1" size="15" title="Enter the terms you wish to search for." class="form-text" /><input type="hidden" name="form_build_id" id="form-f8b63bff527581d147d4696aefa6028a" value="form-f8b63bff527581d147d4696aefa6028a"  />
<input type="hidden" name="form_id" id="edit-search-theme-form" value="search_theme_form"  />
<input type="submit" name="op" class="form-submit" value=" " />
</div></form>
                                </div>
                                                                    <div class="secondary-menu">
                                                                            </div>
                                                            </div>
                        </div>    <div class="head-row3">
                                                            <div class="bg-menu">
                                    
            <!-- YUI Menu div-->
            <div id="productsandservices" class="yuimenubar yuimenubarnav">
              <div class="bd">
                <ul  style="text-decoration:none" class="first-of-type"><li  class="yuimenubaritem"><a href="http://allsportstiming.com/?q=node" class="yuimenubaritemlabel">Home</a></li><li  class="yuimenubaritem"><a href="http://allsportstiming.com/?q=node/62" class="yuimenubaritemlabel">Clients</a></li><li  class="yuimenubaritem"><a href="http://allsportstiming.com/?q=node/61" class="yuimenubaritemlabel">Results</a></li><li  class="yuimenubaritem"><a href="http://allsportstiming.com/?q=node/65" class="yuimenubaritemlabel">Learning</a></li><li  class="yuimenubaritem"><a href="http://allsportstiming.com/?q=node/67" class="yuimenubaritemlabel">Quote Request</a></li><li  class="yuimenubaritem"><a href="http://allsportstiming.com/?q=contact" class="yuimenubaritemlabel">Contact</a></li></ul>
          </div>
       </div>                                </div>
                                                    </div></div></div></div></div</div></div></div>
            ';
            echo "<div class='container'> <br>";
            /*<nav class='navbar navbar-inverse'>
              <div class='container-fluid'>
                <div class='navbar-header'>
                  <a class='navbar-brand' href=' $base_url '>$title</a>
                </div>
                <ul class='nav navbar-nav'>
                  <li";
                  if ($page == 'Home')
                    echo " class='active' ";
                  echo "><a href=' $base_url '>Home</a></li>";
                  if ($page == 'Race')
                    echo "<li class ='active'><a href='$base_url?page=Race&race=".$_GET['race']."''>Results</a></li>";
                  else
                    echo "<li><a href='$base_url'>Results</a></li>";
                  echo "
                </ul>
                <ul class='nav navbar-nav navbar-right'>";
                if( !$isLoggedIn ){
                  echo "<li><a href ='$base_url?page=login'>Login</a></li>";
                }
                else {
                  echo "<li><a href='$base_url?page=logout'>Logout</a></li>";
                }
              echo "</li>
              </div>
            </nav>";*/

}

/******************************************************************************
** Function: printRaces
** Parameters: $db_prefix
** Does: Prints out a list of links to all races in the DB
** TODO: add pagination
*******************************************************************************/
function printRaces( $pdo, $db_prefix ) {
   //query database for basic races information
   $query = "SELECT *  FROM ".$db_prefix."_races";

   echo "<ul id='racelist'>";
   foreach( $pdo->query($query) as $race ){
      $date = date('m/d/y', strtotime( $race['date'] ) );
      echo "<li><a href='?page=Race&race=".$race['tableName']."'>
           ".$race['raceName']." - ".$date."</a></li>";
   }
   echo "</ul>";
}
?>
